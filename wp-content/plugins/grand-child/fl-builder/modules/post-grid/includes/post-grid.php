<?php
$a1 =""; $a2 =[];
if(get_field('gallery_room_images')){
	// loop through the rows of data
	$gallery_images = get_field('gallery_room_images');

	$gallery_img = explode("|",$gallery_images);
	if(count($gallery_img) > 0){

		shuffle($gallery_img);

	foreach($gallery_img as  $key=>$value) {

		
		
		$a2[] =  high_gallery_images_in_loop($value);
        $a1  =  thumb_gallery_images_in_loop($value);
	}
}
}

if($a1 !=""){	
?>
<div <?php post_class('fl-post-grid-post open-gallery-modal'); ?> itemscope itemtype="<?php FLPostGridModule::schema_itemtype(); ?>">
	
	<?php FLPostGridModule::schema_meta(); ?>

	<?php if(has_post_thumbnail() && $settings->show_image) : ?>
	<div class="fl-post-grid-image">
		<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
			<?php the_post_thumbnail($settings->image_size); ?>
		</a>
	</div>
	<?php endif; ?>
	
	<div class="fl-post-grid-gallery" style="background: url('<?php echo $a1; ?>') no-repeat;background-size: cover;" title="<?php the_field('collection'); ?> - <?php the_field('color'); ?>"></div>
	
	<div class="gallery-modal">
		<div class="fl-row-content-wrap" >
			<div class="row">
				<a href="javascript:void(0)" class="close_modal" title="Close"><i class="fa fa-times"></i></a>
				<div class="col col-sm-6 col-md-8 col-lg-8">
					<div class="gallerySlider col_swatch" id="<?php echo rand();?>">
					<?php  for($i=0;$i<count($a2);$i++) { ?>
						<div class="gallery-img-holder"><img alt="<?php the_field('color'); ?>" src="<?php echo $a2[$i];?>"></div>
					<?php } ?>
					</div>
				</div>
				<div class="col col-sm-6 col-md-4 col-lg-4 product-box">					
	                <h3 class="collection"><?php the_field('collection'); ?></h3>
	                <h2 class="fl-post-title" itemprop="name"><?php the_field('color'); ?></h2>

	                <div class="product-colors">
	                    <?php
							global $post;
							$familysku = get_post_meta($post->ID, 'collection', true);	
							$flooringtype =  $post->post_type;							 
							
						?>

                        <?php $args = array(
									'post_type'      => $flooringtype,
									'posts_per_page' => -1,
									'post_status'    => 'publish',
									'meta_query'     => array(
										array(
											'key'     => 'collection',
											'value'   => $familysku,
											'compare' => '='
										)
									)
								);
                            ?>
                        
	                    <?php $the_query = new WP_Query( $args ); ?>
	                    <?php  //echo $the_query ->found_posts; ?> <?php /*?>Colors Available <?php*/ ?>
	                </div>

					<br>
	                <a href="<?php the_permalink() ?>" class="view_more fl-button"><span class="fl-button-text"><?php _e("VIEW PRODUCT","fl-builder"); ?></span></a>
					<br>
	                <?php if( get_option('getcouponbtn') == 1){  ?>
											<a href="<?php if(get_option('getcouponreplace')==1){ echo get_option('getcouponreplaceurl');}else{ echo '/flooring-coupon/'; } ?>" target="_self" class="fl-button gallerypopbtn" role="button" <?php //get_coupon_button_visibility($sale_arr,$brand_arr); ?> >
											<span class="fl-button-text">
											<?php if(get_option('getcouponreplace')==1){ echo get_option('getcouponreplacetext');}else{ echo 'GET COUPON'; }?>
											</span>
											</a>
            		<?php } ?>
				</div>
			</div>
		</div>
	</div>

</div>
<?php } ?>